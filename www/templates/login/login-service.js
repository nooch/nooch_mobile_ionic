angular.module('noochApp.login-service', ['noochApp.services'])
  .service('authenticationService', function ($http) {

      this.Login = function (username, password, remmberMe, lat, lng, deviceId, deviceToken, deviceType) {
          var url = URLs.Login + '?userName=' + username + '&pwd=' + password + '&rememberMeEnabled=' + remmberMe + '&lat=' + lat + '&lng=' + lng + '&udid=' + deviceId + '&devicetoken=' + deviceToken + "&deviceOS=" + deviceType;
          //console.log(url);
          return $http.get(url);
      };

      this.ForgotPassword = function (userName) {
          var data = {
              Input: userName,
              AuthenticationKey: '' // not in use @ server
          };

          return $http.post(URLs.ForgotPassword, data);
      }

      this.LoginWithFb = function (email, FbId, rembMe, lat, lng, deviceId, deviceToken) {
          var url = URLs.LoginWithFb + '?userEmail=' + email + '&FBId=' + FbId + '&rememberMeEnabled=' + rembMe + '&lat=' + lat + '&lng=' + lng + '&udid=' + deviceId + '&devicetoken=' + deviceToken;
          return $http.get(url);
      };

      this.LoginWithTouchId = function (email, lat, lng, deviceId, deviceToken) {
          var url = URLs.LoginWithTouchId + '?email=' + email + '&lat=' + lat + '&lng=' + lng + '&udid=' + deviceId + '&devicetoken=' + deviceToken + '&deviceOS=I';
          console.log(url);
          return $http.get(url);
      };

      this.SaveMembersFBId = function (MemberId, MemberfaceBookId, IsConnect) {
          var url = URLs.SaveMembersFBId + '?memberId=' + MemberId + '&fbid=' + MemberfaceBookId + '&IsConnect=' + IsConnect;
          return $http.get(url);
      };
  })
