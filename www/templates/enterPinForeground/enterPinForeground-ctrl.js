﻿angular.module('noochApp.enterPinForegroundCtrl', ['noochApp.enterPinForeground-service', 'noochApp.services'])

    .controller('enterPinForegroundCtrl', function ($scope, $state, $ionicLoading, $cordovaGoogleAnalytics, $ionicPlatform, $localStorage,
                                                    $timeout, $cordovaTouchID, CommonServices, ValidatePin) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            if (window.StatusBar)
                StatusBar.styleDefault();

            $scope.Pin = '';
            if ($('.indicatorDotWrap .col div').hasClass('filled'))
                $('.indicatorDotWrap .col div').removeClass('filled incorrect');
        });


        $scope.$on("$ionicView.enter", function (event, data) {
            //console.log('Enter Pin Foreground Cntlr loaded');

            $ionicPlatform.ready(function () {
                if (typeof analytics !== 'undefined') analytics.trackView("Enter PIN Foreground");
            })
        });


        $scope.$on("$ionicView.afterEnter", function (event, data) {
            $ionicPlatform.ready(function () {
                CommonServices.checkIfTouchIdAvailable(function (result) {
                    if (result)
                    {
                        $timeout(function () {
                            // Device has TouchID - now check if User has previously enabled it for Nooch
                            if ($localStorage.GLOBAL_VARIABLES.touchId.isEnabled &&
							    $localStorage.GLOBAL_VARIABLES.touchId.requireForLogin)
                            {
                                $scope.promptForTouchId();
                            }
                            else if (!$localStorage.GLOBAL_VARIABLES.touchId.hasBeenPromptedLogin)
                            {
                                swal({
                                    title: "Enable TouchID for Login?",
                                    //text: "Would you like to use TouchID to log in quickly?",
                                    type: "warning",
                                    showCancelButton: true,
                                    cancelButtonText: "No",
                                    confirmButtonText: "Use TouchID"
                                }, function (isConfirm) {
                                    $localStorage.GLOBAL_VARIABLES.touchId.hasBeenPromptedLogin = true;

                                    if (isConfirm)
                                        $scope.promptForTouchId();
                                });
                            }
                        }, 900);
                    }
                });
            })
        });


        $scope.promptForTouchId = function () {
            $cordovaTouchID.authenticate("Please verify your ID.")
	            .then(function () {
	                //console.log("TouchID Authenticated Successfully");
	                $('.indicatorDotWrap .col div').addClass('filled');

	                $localStorage.GLOBAL_VARIABLES.touchId.isEnabled = true;
	                $localStorage.GLOBAL_VARIABLES.touchId.requireForLogin = true;

	                $scope.goToDestination();
	            }, function (error) {
	                console.log(JSON.stringify(error));
	            });
        }


        $scope.numTapped = function (num) {
            if ($scope.pauseForError != true)
            {
                if ($('.instructionTxt').hasClass('expanded'))
                    $('.instructionTxt').html("").removeClass('expanded');

                if (num < 10)
                {
                    $scope.Pin += num;

                    if ($scope.Pin.length < 4)
                    {
                        if ($scope.Pin.length == 1)
                            $('.indicatorDotWrap .col div:first-child').addClass('filled');
                        if ($scope.Pin.length == 2)
                            $('.indicatorDotWrap .col div:nth-child(2)').addClass('filled');
                        if ($scope.Pin.length == 3)
                            $('.indicatorDotWrap .col div:nth-child(3)').addClass('filled');
                    }
                    else
                    {
                        $('.indicatorDotWrap .col div:last-child').addClass('filled');

                        $ionicLoading.show({
                            template: 'Checking PIN...'
                        });

                        CommonServices.GetEncryptedData($scope.Pin)
	                        .success(function (data) {
	                            // Reset PIN value no matter what happens with validation below
	                            $scope.Pin = '';

	                            ValidatePin.ValidatePinNumberToEnterForEnterForeground(data.Status)
                                    .success(function (response) {
                                        $ionicLoading.hide();

                                        if (response.Result == 'Success')
                                        {
                                            $scope.goToDestination();
                                        }
                                        else if (response.Result != null && response.Result.indexOf('will be suspended') > -1)
                                        {
                                            $scope.applyError();
                                            swal({
                                                title: "Careful...",
                                                text: "To keep Nooch safe, your account will be suspended if you enter another incorrect PIN.",
                                                type: "warning",
                                                customClass: "singleBtn"
                                            });
                                        }
                                        else if (response.Result != null && response.Result.indexOf('suspended') > -1)
                                        {
                                            $scope.applyError();
                                            swal({
                                                title: "Account Suspended",
                                                text: "To keep Nooch safe, your account has been temporarily suspended because you entered an incorrect PIN too many times." +
	                                                  "<span class='show'>In most cases your account will be automatically un-suspended in 24 hours.</span>",
                                                type: "error",
                                                showCancelButton: true,
                                                cancelButtonText: "Ok",
                                                confirmButtonText: "Contact Support",
                                                html: true,
                                                customClass: "singleBtn"
                                            }, function (isConfirm) {
                                                if (isConfirm)
                                                {
                                                    $cordovaSocialSharing.shareViaEmail('', 'Nooch Support Request - Account Suspended', 'support@nooch.com', null, null, null)
	                                                    .then(function (res) {
	                                                        $state.go('login');
	                                                    }, function (err) {
	                                                        $state.go('login');
	                                                    });
                                                }
                                                else
                                                    $state.go('login');
                                            });
                                        }
                                        else if (response.Result != "Success")
                                            $scope.applyError();
                                    })
                                    .error(function (error) {
                                        console.log(error);
                                        $ionicLoading.hide();

                                        if (error != null && error.ExceptionMessage == 'Invalid OAuth 2 Access')
                                            CommonServices.logOut();
                                        else
                                        {
                                            CommonServices.DisplayError('Unable to login right now :-(');
                                            $scope.applyError();
                                        }
                                    });
	                        })
	                        .error(function (error) {
	                            CommonServices.DisplayError('Unable to login right now.');
	                            console.log('NumTapped -> GetEncryptedData Error: [' + error + ']');
	                        });
                    }
                }
                else if (num == 10)
                {
                    if ($scope.Pin.length > 0)
                    {
                        $scope.Pin = $scope.Pin.slice(0, -1);

                        if ($scope.Pin.length == 0)
                            $('.indicatorDotWrap .col div:first-child').removeClass('filled');
                        if ($scope.Pin.length == 1)
                            $('.indicatorDotWrap .col div:nth-child(2)').removeClass('filled');
                        if ($scope.Pin.length == 2)
                            $('.indicatorDotWrap .col div:nth-child(3)').removeClass('filled');
                        if ($scope.Pin.length == 3)
                            $('.indicatorDotWrap .col div:last-child').removeClass('filled');
                    }
                }
            }
        }


        $scope.goToDestination = function () {
            var sendTo = ($localStorage.GLOBAL_VARIABLES.lastScreenAtAppPause != undefined &&
                          $localStorage.GLOBAL_VARIABLES.lastScreenAtAppPause != '')
                            ? $localStorage.GLOBAL_VARIABLES.lastScreenAtAppPause
                            : 'app.home';

            // Transfer Details causes issues with the view history, so just go to main History Scrn instead
            if (sendTo == "app.transferDetails") sendTo = "app.history";

            $state.go(sendTo);
        }


        $scope.applyError = function () {
            $scope.pauseForError = true;

            $('.indicatorDotWrap .col div').addClass('incorrect');
            $('.indicatorDotWrap').addClass('shake');
            $('.instructionTxt').html("Incorrect PIN! &nbsp;Try again...").addClass('expanded');

            $timeout(function () {
                $('.indicatorDotWrap').removeClass('shake');
                $('.indicatorDotWrap .col div').removeClass('filled incorrect');
                $scope.pauseForError = false;
            }, 1200);
        }

        $scope.$on("$ionicView.leave", function (event, data) {
            if (window.StatusBar)
                StatusBar.styleLightContent();
        });
    });
