﻿angular.module('noochApp.historyCtrl', ['noochApp.history-service', 'noochApp.services'])

    .controller('historyCtrl', function ($scope, $filter, historyService, $cordovaGoogleAnalytics, $ionicPlatform, $ionicLoading, $localStorage,
                                         $ionicListDelegate, $rootScope, $ionicContentBanner, $state, $stateParams, $ionicHistory, $ionicActionSheet,
                                         transferDetailsService, CommonServices, ValidatePin) {

        $scope.$on("$ionicView.enter", function (event, data) {
            console.log('History Page Loaded');

            $scope.historyListHeight = { 'height': $rootScope.screenHeight - 158 + 'px' }
            $scope.firstTimeDivHeight = { 'min-height': $rootScope.screenHeight - 154 + 'px' }

            $scope.transDetailsForPin = {};
            var transDetails = {};

            //console.log($scope.transactionList);

            if (typeof $scope.transactionList == 'undefined')
            {
                $scope.isFinishedLoading = false;
                $scope.transactionList = [];

                if ($stateParams.preloadType != null && $stateParams.preloadType == "pending")
                {
                    $scope.completed = false;
                    $scope.pending = true;
                    $('#btnCompleted').removeClass('active');
                    $('#btnPending').addClass('active');
                    $scope.getTransactions('Pending')
                }
                else
                {
                    $scope.completed = true;
                    $scope.pending = false;
                    $('#btnCompleted').addClass('active');
                    $('#btnPending').removeClass('active');
                    $scope.getTransactions('');
                }
            }

            $rootScope.Location = {
                longi: '',
                lati: ''
            }

            $ionicPlatform.ready(function () {
                if (typeof analytics !== 'undefined') analytics.trackView("History Screen");
            })
        });


        $scope.getTransactions = function (type) {
            $ionicLoading.show({
                template: 'Loading Payment History...'
            });

            historyService.getTransferList(type)
				.success(function (data) {
				    $scope.isFinishedLoading = true;

				    $scope.transactionList = data;
				    $scope.filterFlag = data;
				    //console.log('GetTransferList Result Data >>>>>');
				    //console.log($scope.transactionList);

				    var pendingCount = 0;

				    for (var i = 0; i < $scope.transactionList.length; i++)
				    {
				        $scope.transactionList[i].TransactionDate = new Date($scope.transactionList[i].TransactionDate);

				        if ($scope.transactionList[i].TransactionStatus == 'Pending')
				            pendingCount += 1;
				    }

				    //if ($rootScope.pendingTransfersCount == null || pendingCount > $rootScope.pendingTransfersCount)
				    $rootScope.pendingTransfersCount = pendingCount;

				    $scope.transList = $scope.transactionList;
				    //$scope.memberId = $localStorage.GLOBAL_VARIABLES.MemberId;

				    $ionicLoading.hide();
				})
				.error(function (error) {
				    console.log('History Cntrl -> GetTransferList Error: [' + JSON.stringify(error) + ']');

				    $scope.isFinishedLoading = true;

				    $ionicLoading.hide();

				    if (error.ExceptionMessage == 'Invalid OAuth 2 Access')
				        CommonServices.logOut();
				});
        }


        $scope.cancelPayment = function (trans) {
            //console.log("Cancel Payment: [" + trans.TransactionId + ']');
            console.log(trans);

            var otherUser = trans.Name != null ? " to " + trans.Name : "";
            var bodyText = "Are you sure you want to cancel this request" + otherUser + "?";

            swal({
                title: "Cancel Request?",
                text: bodyText,
                type: "warning",
                confirmButtonText: "Yes",
                showCancelButton: true,
            }, function (isConfirm) {
                $ionicListDelegate.closeOptionButtons();
                if (isConfirm)
                {
                    $ionicLoading.show({
                        template: 'Cancelling Request...'
                    });

                    if (trans.MemberId == trans.RecepientId)
                    {
                        transferDetailsService.CancelMoneyRequestForNonNoochUser(trans.TransactionId)
                            .success(function (data) {
                                console.log(data);
                                $ionicLoading.hide();
                                if (data.Result.indexOf('Successfully') > -1)
                                {
                                    swal({
                                        title: "Request Cancelled Successfully",
                                        type: "success",
                                        customClass: "singleBtn"
                                    }, function () {
                                        $scope.getTransactions('Pending');
                                    });
                                }
                                else
                                    CommonServices.DisplayError('Unable to cancel that payment!');
                            })
                            .error(function (data) {
                                $ionicLoading.hide();
                                if (data.ExceptionMessage == 'Invalid OAuth 2 Access')
                                    CommonServices.logOut();
                                else
                                    CommonServices.DisplayError('Unable to cancel that payment!');
                            });
                    }
                    else
                    {
                        transferDetailsService.CancelMoneyRequestForExistingNoochUser(trans.TransactionId)
                            .success(function (data) {
                                console.log(data);
                                $ionicLoading.hide();
                                if (data.Result.indexOf('Successfully') > -1)
                                {
                                    swal({
                                        title: "Request Cancelled Successfully",
                                        type: "success",
                                        customClass: "singleBtn"
                                    }, function () {
                                        $scope.getTransactions('Pending');
                                    });
                                }
                                else
                                    CommonServices.DisplayError('Unable to cancel that payment!');
                            })
                            .error(function (data) {
                                $ionicLoading.hide();
                                if (data.ExceptionMessage == 'Invalid OAuth 2 Access')
                                    CommonServices.logOut();
                                else
                                    CommonServices.DisplayError('Unable to cancel that payment!');
                            });
                    }
                }
            });
        }


        $scope.rejectPayment = function (trans) {

            console.log("Reject Payment Fired: [" + trans.TransactionId + ']');

            swal({
                title: "Reject Payment",
                text: "Are you sure you want to <strong>reject</strong> " + trans.Name + "'s payment request?",
                type: "warning",
                confirmButtonText: "Yes",
                showCancelButton: true,
                html: true
            }, function (isConfirm) {
                $ionicListDelegate.closeOptionButtons();
                if (isConfirm)
                {
                    $ionicLoading.show({
                        template: 'Rejecting Request...'
                    });

                    transferDetailsService.RejectPayment(trans.TransactionId)
                        .success(function (data) {
                            $ionicLoading.hide();

                            if (data.Result.indexOf('Successfully') > -1)
                            {
                                swal({
                                    title: "Request Rejected Successfully",
                                    type: "success",
                                    customClass: "singleBtn"
                                }, function () {
                                    $scope.getTransactions('Pending');
                                });
                            }
                        })
                        .error(function (error) {
                            CommonServices.DisplayError('Request Not Rejected');
                            $ionicLoading.hide();
                            if (error.ExceptionMessage == 'Invalid OAuth 2 Access')
                                CommonServices.logOut();
                        });
                }
            })
        }


        $scope.remindPayment = function (trans) {

            swal({
                title: "Send Reminder",
                text: "Do you want to send a reminder about this request?",
                type: "warning",
                confirmButtonText: "Yes",
                showCancelButton: true,
            }, function (isConfirm) {
                $ionicListDelegate.closeOptionButtons();
                if (isConfirm)
                {
                    $ionicLoading.show({
                        template: 'Sending Reminder...'
                    });

                    transferDetailsService.RemindPayment(trans.TransactionId)
		                .success(function (data) {
		                    console.log(data);
		                    $ionicLoading.hide();
		                    if (data.Result.indexOf('successfully') > -1)
		                        swal({
		                            title: "Reminder Sent Successfully",
		                            type: "success",
		                            customClass: "singleBtn"
		                        });
		                    else
		                        swal("Error", data.Result, "error");
		                })
		                .error(function (error) {
		                    $ionicLoading.hide();
		                    if (error.ExceptionMessage == 'Invalid OAuth 2 Access')
		                        CommonServices.logOut();
		                });
                }
            });
        }


        $scope.PayBack = function (trans) {
            console.log("Pay Back Result: [" + JSON.stringify(trans) + ']');
            $state.go('app.howMuch', { myParam: trans });
        }


        $scope.TransferMoney = function (trans) {

            swal({
                title: "Pay Request?",
                text: "Would you like to pay this request to " + trans.Name + " now?",
                type: "warning",
                confirmButtonText: "Yes",
                showCancelButton: true,
            }, function (isConfirm) {
                if (isConfirm)
                {
                    transDetails = trans;
                    trans.Name.RecepientName = trans.Name;
                    //console.log("TransDetails: [" + JSON.stringify(transDetails) + ']');

                    CommonServices.savePinValidationScreenData({ transObj: transDetails, type: 'transfer', returnUrl: 'app.history', returnPage: 'History', comingFrom: 'Transfer' });

                    $state.go('enterPin');
                }
            });
        }


        $scope.showMap = function (longi, lati) {
            //console.log($rootScope.Location.longi);
            //console.log($rootScope.Location.lati);

            $ionicListDelegate.closeOptionButtons();

            if (longi == 0 || lati == 0 || longi == '' || lati == '')
            {
                $ionicContentBanner.show({
                    text: ['No Location Found'],
                    autoClose: '3000',
                    type: 'error',
                    transition: 'vertical'
                });
            }
            else
            {
                $ionicLoading.show({
                    template: 'Loading Payment Location...'
                });

                $rootScope.Location.longi = longi;
                $rootScope.Location.lati = lati;
                $state.go('app.map');
                $ionicLoading.hide();
            }
        }


        $scope.toggleView = function (view) {
            $ionicHistory.clearCache().then(function () {
                if (view == 'completed')
                {
                    $scope.getTransactions('');

                    $scope.completed = true;
                    $scope.pending = false;
                }
                else
                {
                    $scope.getTransactions('Pending');

                    $scope.pending = true;
                    $scope.completed = false;
                }
            });
        }


        $scope.openFilterChoices = function () {
            $('#btnCompleted').attr('disabled', true);
            $('#btnPending').attr('disabled', true);

            var title = $scope.completed == true ? "Filter Options (Completed Payments)" : "Filter Options (Pending Payments)";
            var buttons = [];
            if ($scope.completed == true)
            {
                buttons = [
                    { text: 'Sent' },
                    { text: 'Received' },
                    { text: 'Cancelled' },
                    { text: 'Rejected' },
                    { text: 'Disputed' },
                    { text: 'ALL' }
                ]
            }
            else
            {
                buttons = [
                    { text: 'Sent' },
                    { text: 'Received' },
                    { text: 'Disputed' },
                    { text: 'ALL' }
                ]
            }

            var hideSheet = $ionicActionSheet.show({
                buttons: buttons,
                titleText: title,
                cancelText: 'Cancel',
                cancel: function () {
                    $('#btnCompleted').attr('disabled', false);
                    $('#btnPending').attr('disabled', false);
                },
                buttonClicked: function (index) {
                    var filter = "";
                    if (index == 0)
                    {
                        if ($scope.completed == true) filter = 'SentC';
                        else filter = 'SentP';
                    }
                    else if (index == 1)
                    {
                        if ($scope.completed == true) filter = 'ReceivedC';
                        else filter = 'ReceivedP';
                    }
                    else if (index == 2)
                    {
                        if ($scope.completed == true) filter = 'Cancelled';
                        else filter = 'DisputedP';
                    }
                    else if (index == 3)
                    {
                        if ($scope.completed == true) filter = 'Rejected';
                        else filter = "AllP"
                    }
                    else if (index == 4)
                    {
                        filter = 'DisputedC';
                    }
                    else// if (index == 5)
                    {
                        filter = 'AllC';
                    }

                    $scope.setFilter(filter);

                    return true;
                }
            });
        };


        $scope.setFilter = function (type) {
            console.log(type);

            if (type == 'AllC')
                $scope.getTransactions('');
            else if (type == 'AllP')
                $scope.getTransactions('Pending');
            else
            {
                var filteredList = [];

                $scope.transactionList = $scope.transList;

                var length = ($scope.transactionList.length);

                for (var i = 0; i < length; i++)
                {
                    if (type == 'SentC')
                    {
                        if ($scope.transactionList[i].TransactionStatus == 'Success' &&
	                        $scope.transactionList[i].MemberId == $scope.memberId &&
	                        ($scope.transactionList[i].TransactionType == 'Transfer' || $scope.transactionList[i].TransactionType == 'Request'))
                            filteredList.push($scope.transactionList[i]);
                    }
                    else if (type == 'SentP')
                    {
                        if ($scope.transactionList[i].TransactionStatus == 'Pending' &&
	                        (($scope.transactionList[i].TransactionType == 'Request' && $scope.transactionList[i].RecepientId == $scope.memberId) ||
	                        ($scope.transactionList[i].TransactionType == 'Invite' && $scope.transactionList[i].MemberId == $scope.memberId)))
                            filteredList.push($scope.transactionList[i]);
                    }
                    else if (type == 'ReceivedC')
                    {
                        if ($scope.transactionList[i].TransactionStatus == 'Success' &&
	                        $scope.transactionList[i].MemberId != $scope.memberId &&
	                        ($scope.transactionList[i].TransactionType == 'Transfer' || $scope.transactionList[i].TransactionType == 'Request'))
                            filteredList.push($scope.transactionList[i]);
                    }
                    else if (type == 'ReceivedP') // can't receive a Pending 'Transfer' - therefore this can only be requests
                    {
                        if ($scope.transactionList[i].TransactionStatus == 'Pending' &&
	                        $scope.transactionList[i].TransactionType == 'Request' &&
	                        $scope.transactionList[i].MemberId == $scope.memberId &&
	                        $scope.transactionList[i].InvitationSentTo == null)
                            filteredList.push($scope.transactionList[i]);
                    }
                    else if (type == 'Cancelled')
                    {
                        if ($scope.transactionList[i].TransactionStatus == 'Cancelled')
                            filteredList.push($scope.transactionList[i]);
                    }
                    else if (type == 'Rejected')
                    {
                        if ($scope.transactionList[i].TransactionStatus == 'Rejected')
                            filteredList.push($scope.transactionList[i]);
                    }
                    else if (type == 'DisputedC')
                    {
                        if ($scope.transactionList[i].TransactionType == 'Disputed' && $scope.transactionList[i].DisputeStatus == 'Resolved')
                            filteredList.push($scope.transactionList[i]);
                    }
                    else if (type == 'DisputedP')
                    {
                        if ($scope.transactionList[i].TransactionType == 'Disputed' &&
	                        $scope.transactionList[i].DisputeStatus == 'Under Review')
                            filteredList.push($scope.transactionList[i]);
                    }
                }
                //console.log(filteredList);

                $scope.transactionList = filteredList;
            }

            $('#btnCompleted').attr('disabled', false);
            $('#btnPending').attr('disabled', false);
        }


        $scope.$watch('search', function (val) {
            //console.log("SEARCH FIRED");
            //console.log($filter('filter')($scope.transactionList, val));

            $scope.transactionList = $filter('filter')($scope.transactionList, val);

            ///console.log($scope.transactionList);

            if ($('#searchBar').val().length == 0)
                $scope.transactionList = $scope.transList;
        });

    });