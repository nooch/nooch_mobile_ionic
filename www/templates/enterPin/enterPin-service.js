﻿angular.module('noochApp.enterPin-service', ['noochApp.services'])
  .service('enterPinService', function ($http, $localStorage) {

      this.TransferMoney = function (Data) {
          console.log(Data);

          var ResTransferPayment = {
              method: 'POST',
              url: URLs.TransferMoney + '?accessToken=' + $localStorage.GLOBAL_VARIABLES.AccessToken,
              headers: {
                  'Content-Type': 'application/json'
              },
              data: Data
          };

          return $http(ResTransferPayment);
      };

      this.TransferMoneyToNonNoochUserUsingSynapse = function (Data) {
          console.log(Data);

          var ResTransferPayment = {
              method: 'POST',
              url: URLs.TransferMoneyToNonNoochUserUsingSynapse + '?accessToken=' + $localStorage.GLOBAL_VARIABLES.AccessToken + '&inviteType=Email&' + 'receiverEmailId=' + Data.InvitationSentTo,
              headers: {
                  'Content-Type': 'application/json'
              },
              data: Data
          };

          return $http(ResTransferPayment);
      };

      this.TransferMoneyToNonNoochUserThroughPhoneUsingsynapse = function (Data) {
          console.log(Data);

          var ResTransferPayment = {
              method: 'POST',
              url: URLs.TransferMoneyToNonNoochUserThroughPhoneUsingsynapse + '?accessToken=' + $localStorage.GLOBAL_VARIABLES.AccessToken + '&inviteType=email&receiverPhoneNumer=' + Data.PhoneNumberInvited,
              headers: {
                  'Content-Type': 'application/json'
              },

              data: Data
          };

          return $http(ResTransferPayment);
      };

      this.RequestMoney = function (Data) {
          // Data.MemberId = $localStorage.GLOBAL_VARIABLES.MemberId;

          var ResTransferPayment = {
              method: 'POST',
              url: URLs.RequestMoney + '?accessToken=' + $localStorage.GLOBAL_VARIABLES.AccessToken,
              headers: {
                  'Content-Type': 'application/json'
              },
              data: Data
          };

          return $http(ResTransferPayment);
      };

      this.RequestMoneyToNonNoochUserUsingSynapse = function (Data) {
          // Data.MemberId = $localStorage.GLOBAL_VARIABLES.MemberId;
          var ResTransferPayment = {
              method: 'POST',
              url: URLs.RequestMoneyToNonNoochUserUsingSynapse + '?accessToken=' + $localStorage.GLOBAL_VARIABLES.AccessToken,
              headers: {
                  'Content-Type': 'application/json'
              },
              data: Data
          };

          return $http(ResTransferPayment);
      };

      this.RequestMoneyToNonNoochUserThroughPhoneUsingSynapse = function (Data, contactNum) {
          // Data.MemberId = $localStorage.GLOBAL_VARIABLES.MemberId;
          var ResTransferPayment = {
              method: 'POST',
              url: URLs.RequestMoneyToNonNoochUserThroughPhoneUsingSynapse + '?accessToken=' + $localStorage.GLOBAL_VARIABLES.AccessToken + '&PayorPhoneNumber=' + contactNum,
              headers: {
                  'Content-Type': 'application/json'
              },

              data: Data
          };

          return $http(ResTransferPayment);
      };

      this.submit2fa = function (pin) {
          var url = URLs.Submit2fa + '?memberId=' + $localStorage.GLOBAL_VARIABLES.MemberId + '&pin=' + pin;
          console.log('EnterPIN Service -> Submit 2FA -> ' + url);
          return $http.get(url);
      };
  })