﻿angular.module('noochApp.enterPin', ['noochApp.services', 'noochApp.enterPin-service'])

    .controller('enterPinCtrl', function ($scope, $rootScope, $state, $stateParams, $localStorage, $ionicHistory, $ionicLoading, $cordovaSocialSharing,
										  $cordovaTouchID, $cordovaGoogleAnalytics, $ionicPlatform, $timeout, CommonServices, ValidatePin,
										  transferDetailsService, howMuchService, enterPinService) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            //console.log('Enter Pin Controller loaded');
            var obj = CommonServices.getPinValidationScreenData();

            $scope.Details = obj.transObj;
            $scope.Details.enterPin = '';
            $scope.memId = $localStorage.GLOBAL_VARIABLES.MemberId;
            $scope.returnUrl = obj.returnUrl;
            $scope.returnPage = obj.returnPage;
            $scope.type = obj.type;
            $scope.attachedPicForLocalDisplay = null;

            console.log($scope.Details);

            if ($scope.Details.Picture != null)
            {
                // CC (9/14/16): The How Much Scrn sends the Base64 version to CommonServices.getPinValidationScreenData()...
                // THAT version is what we display here for the Enter PIN screen...
                // But we strip out the Base64 prefix stuff to send to the server (I think)
                $scope.attachedPicForLocalDisplay = $scope.Details.Picture;
                $scope.Details.Picture = ($scope.Details.Picture.replace(/^data:image\/(png|jpg|jpeg);base64,/, ""));
            }

            $ionicPlatform.ready(function () {
                if (typeof analytics !== 'undefined') analytics.trackView("Enter PIN");
            })
        });


        $scope.$on("$ionicView.afterEnter", function (event, data) {
            $ionicPlatform.ready(function () {

                CommonServices.checkIfTouchIdAvailable(function (result) {
                    if (result)
                    {
                        $timeout(function () {
                            // Device has TouchID - now check if User has previously enabled it for Nooch
                            if ($localStorage.GLOBAL_VARIABLES.touchId.isEnabled &&
	                            $localStorage.GLOBAL_VARIABLES.touchId.requireForPayments)
                            {
                                $scope.promptForTouchId();
                            }
                            else if (!$localStorage.GLOBAL_VARIABLES.touchId.hasBeenPromptedPayment)
                            {
                                swal({
                                    title: "Enable TouchID for Payments?",
                                    //text: "Would you like to use TouchID to log in quickly?",
                                    type: "warning",
                                    showCancelButton: true,
                                    cancelButtonText: "No",
                                    confirmButtonText: "Use TouchID"
                                }, function (isConfirm) {
                                    $localStorage.GLOBAL_VARIABLES.touchId.hasBeenPromptedPayment = true;

                                    if (isConfirm)
                                        $scope.promptForTouchId();
                                });
                            }
                        }, 900);
                    }
                });
            })
        });


        $scope.promptForTouchId = function () {
            $cordovaTouchID.authenticate("Please verify your ID to complete this payment.")
	            .then(function () {
	                // Success --> Log the user in
	                //console.log("TouchID Authenticated Successfully");
	                $localStorage.GLOBAL_VARIABLES.touchId.isEnabled = true;
	                $localStorage.GLOBAL_VARIABLES.touchId.requireForPayments = true;

	                $scope.Details.PinNumber = $rootScope.pinEnc;

	                $ionicLoading.show({
	                    template: 'Submitting Payment...'
	                });

	                $scope.completePayment();
	            }, function (error) {
	                console.log(JSON.stringify(error));
	            });
        }


        $scope.keyboardSettings = {
            action: function (number) {
                $scope.Details.enterPin += number.toString();
                $scope.processPin();
            },
            leftButton: {
                html: '<i class="icon ion-backspace"></i>',
                action: function () {
                    $scope.Details.enterPin = $scope.Details.enterPin.slice(0, -1);

                    if ($scope.Details.enterPin.length == 0)
                        $('.indicatorDotWrap .col div').removeClass('filled');
                    if ($scope.Details.enterPin.length == 1)
                        $('.indicatorDotWrap .col div:nth-child(2)').removeClass('filled');
                    if ($scope.Details.enterPin.length == 2)
                        $('.indicatorDotWrap .col div:nth-child(3)').removeClass('filled');
                    if ($scope.Details.enterPin.length == 3)
                        $('.indicatorDotWrap .col div:last-child').removeClass('filled');
                }
            }
        }


        $scope.processPin = function () {
            var Pin = $scope.Details.enterPin;
            //console.log(Pin);

            if (Pin.length < 4)
            {
                if (Pin.length == 1)
                    $('.indicatorDotWrap .col div:first-child').addClass('filled');
                if (Pin.length == 2)
                    $('.indicatorDotWrap .col div:nth-child(2)').addClass('filled');
                if (Pin.length == 3)
                    $('.indicatorDotWrap .col div:nth-child(3)').addClass('filled');
            }
            else// if (Pin.length == 4)
            {
                $('.indicatorDotWrap .col div:last-child').addClass('filled');

                $ionicLoading.show({
                    template: 'Submitting Payment...'
                });

                // console.log(JSON.stringify($scope.Details));

                CommonServices.GetEncryptedData(Pin).success(function (encrPin) {

                    $scope.Details.PinNumber = encrPin.Status;

                    ValidatePin.ValidatePinNumberToEnterForEnterForeground($scope.Details.PinNumber)
	                   .success(function (data) {
	                       console.log(data);

	                       if (data.Result == 'Success')
	                       {
	                           $scope.completePayment();
	                       }
	                       else if (data.Result == 'Invalid Pin')
	                       {
	                           $ionicLoading.hide();

	                           $('.indicatorDotWrap .col div').addClass('incorrect');
	                           $('.indicatorDotWrap').addClass('shake');

	                           swal({
	                               title: "Incorrect PIN",
	                               text: "Looks like your PIN wasn't quite right! Please try again.",
	                               type: "error",
	                               customClass: "singleBtn",
	                           }, function () {
	                               $scope.Details.enterPin = '';
	                               $('.indicatorDotWrap').removeClass('shake');
	                               $('.indicatorDotWrap .col div').removeClass('filled incorrect');
	                           });
	                       }
	                       else// if (data.Message == 'An error has occurred.')
	                       {
	                           $ionicLoading.hide();
	                           $scope.showErrorAlert('');
	                       }
	                   })
					   .error(function (error) {
					       console.log('Enter PIN -> ValidatePinNumberToEnterForEnterForeground Error [' + JSON.strigify(error) + ']');
					       $ionicLoading.hide();

					       if (data.ExceptionMessage == 'Invalid OAuth 2 Access')
					           CommonServices.logOut();
					   });
                })
				.error(function (error) {
				    $ionicLoading.hide();

				    if (error.ExceptionMessage == 'Invalid OAuth 2 Access')
				        CommonServices.logOut();
				});
            }
        }


        $scope.completePayment = function () {
            //if ($scope.returnUrl == 'app.home')
            //    $ionicHistory.clearCache().then(function () { $state.go($scope.returnUrl); });

            $scope.Details.Latitude = $localStorage.GLOBAL_VARIABLES.UserCurrentLatitude;
            $scope.Details.Longitude = $localStorage.GLOBAL_VARIABLES.UserCurrentLongi;

            if ($scope.type == 'transfer')
            {
                if ($scope.Details.MemberId != $scope.Details.RecepientId &&
				    $scope.Details.RecepientId != null &&
				    $scope.Details.RecepientId != undefined)
                {
                    enterPinService.TransferMoney($scope.Details)
					   .success(function (data) {
					       if (data.Result == 'Your cash was sent successfully')
					           $scope.showSuccessAlert('send');
					       else
					           $scope.showErrorAlert(data.Result);
					   })
					   .error(function (error) {
					       console.log("TransferMoney Error: [" + JSON.strigify(error) + "]");
					       $ionicLoading.hide();
					       swal("Error", error, "error");
					   });
                }
                else if (($scope.Details.MemberId == $scope.Details.RecepientId ||
				   		 $scope.Details.RecepientId == null ||
				   		 $scope.Details.RecepientId == undefined) &&
				   		 $scope.Details.InvitationSentTo != undefined)
                {
                    enterPinService.TransferMoneyToNonNoochUserUsingSynapse($scope.Details)
                     .success(function (data) {
                         if (data.Result == 'Your cash was sent successfully')
                             $scope.showSuccessAlert('send');
                         else
                             $scope.showErrorAlert(data.Result);
                     })
                     .error(function (error) {
                         console.log("TransferMoneyToNonNoochUserUsingSynapse Error: [" + JSON.strigify(error) + "]");
                         $ionicLoading.hide();
                         swal("Error", error, "error");
                     });
                }
                else if (($scope.Details.MemberId == $scope.Details.RecepientId ||
				   		 $scope.Details.RecepientId == null ||
				   		 $scope.Details.RecepientId == undefined) &&
				   		 $scope.Details.PhoneNumberInvited != undefined)
                {
                    enterPinService.TransferMoneyToNonNoochUserThroughPhoneUsingsynapse($scope.Details)
                        .success(function (data) {
                            if (data.Result == 'Your cash was sent successfully')
                                $scope.showSuccessAlert('send');
                            else
                                $scope.showErrorAlert(data.Result);
                        })
                        .error(function (error) {
                            console.log("TransferMoneyToNonNoochUserThroughPhoneUsingsynapse Error: [" + JSON.strigify(error) + "]");
                            $ionicLoading.hide();
                            swal("Error", error, "error");
                        });
                }
            }

            else if ($scope.type == 'request')
            {
                if ($scope.Details.SenderId != null)
                {
                    console.log($scope.Details);

                    enterPinService.RequestMoney($scope.Details)
                        .success(function (data) {

                            if (data.Result.indexOf('successfully') > -1)
                                $scope.showSuccessAlert('request');
                            else
                                $scope.showErrorAlert(data.Result);
                        })
                        .error(function (error) {
                            console.log("RequestMoney Error: [" + JSON.strigify(error) + "]");
                            $ionicLoading.hide();
                            swal("Error", error, "error");

                            if (error.ExceptionMessage == 'Invalid OAuth 2 Access')
                                CommonServices.logOut();
                        });
                }
                else if (($scope.Details.SenderId == null || $scope.Details.SenderId == undefined) &&
			   			 $scope.Details.MoneySenderEmailId != null &&
			   			 $scope.Details.ContactNumber == null)
                {
                    enterPinService.RequestMoneyToNonNoochUserUsingSynapse($scope.Details)
                        .success(function (data) {

                            if (data.Result.indexOf('successfully') > -1)
                                $scope.showSuccessAlert('request');
                            else
                                $scope.showErrorAlert(data.Result);
                        })
                        .error(function (data) {
                            console.log("RequestMoneyToNonNoochUserUsingSynapse Error: [" + JSON.strigify(error) + "]");
                            $ionicLoading.hide();
                            swal("Error", error, "error");

                            if (error.ExceptionMessage == 'Invalid OAuth 2 Access')
                                CommonServices.logOut();
                        });
                }
                else if ($scope.Details.SenderId == null && $scope.Details.contactNumber != null)
                {
                    enterPinService.RequestMoneyToNonNoochUserThroughPhoneUsingSynapse($scope.Details, $scope.Details.contactNumber)
                        .success(function (data) {

                            if (data.Result.indexOf('successfully') > -1)
                                $scope.showSuccessAlert('request');
                            else
                                $scope.showErrorAlert(data.Result);
                        })
                        .error(function (error) {
                            console.log("RequestMoneyToNonNoochUserThroughPhoneUsingSynapse Error: [" + JSON.strigify(error) + "]");
                            $ionicLoading.hide();
                            swal("Error", error, "error");

                            if (error.ExceptionMessage == 'Invalid OAuth 2 Access')
                                CommonServices.logOut();
                        });
                }
            }
        }


        $scope.showSuccessAlert = function (type) {
            $ionicLoading.hide();

            $scope.Details = "";

            var title = "";
            var msg = "";

            if (type == 'request')
            {
                title = "Request Sent";
                msg = "Your payment request has been sent succesfully.";
            }
            else
            {
                title = "Payment Sent";
                msg = "Your payment has been sent successfully.";
            }

            swal({
                title: title,
                text: msg,
                type: "success",
                showCancelButton: true,
                confirmButtonText: "View Details",
                cancelButtonText: "Ok",
            }, function (isConfirm) {
                if (isConfirm || $scope.returnUrl == 'app.transferDetails')
                {
                    // $state.go('app.transferDetails');
                    var historyList = type == 'request' ? 'pending' : 'completed';

                    $ionicHistory.clearCache().then(function () {
                        $state.go('app.history', { preloadType: historyList });
                    });
                }
                else
                {
                    $ionicHistory.clearCache().then(function () {
                        $state.go('app.home');
                    });
                }
            });
        }


        $scope.showErrorAlert = function (msg) {
            $ionicLoading.hide();

            var errorTitle = "";
            var errorMsg = "";
            var goTo = "home";

            console.log(msg);

            if (msg.indexOf('Whoa now big spender') > -1)
            {
                errorTitle = "High Roller Alert";
                errorMsg = msg;
            }
            else if (msg.indexOf('Weekly transfer limit exceeded') > -1)
            {
                errorTitle = "Weekly Transfer Limit Reached";
                errorMsg = "Looks like you have already exceeded your weekly transaction limit. If this is an error or you'd like more information, please contact support@nooch.com.";
            }
            else if (msg.indexOf('Sender user details not found') > -1 || msg.indexOf('Sender have not linked to any bank account') > -1)
            {
                errorTitle = "Account Not Fully Setup";
                errorMsg = "Looks like your bank account is not fully linked yet to your Nooch account. Please go to settings and confirm that your bank account is linked and verified.";
                goTo = "settings";
            }
            else if (msg.indexOf('Sender has insufficient permissions: [UNVERIFIED]') > -1)
            {
                errorTitle = "ID Unverified";
                errorMsg = "Looks like your account remains unverified. This usually occurs if we are unable to verify your ID and can usually be fixed quickly by contacting support@nooch.com for additional info. Sorry for the inconvenience!";
                goTo = "settings";
            }
            else if (msg.indexOf('Sender has insufficient permissions: [CREDIT]') > -1)
            {
                errorTitle = "Permission Error";
                errorMsg = "Looks like your account is only verified to receive payments. To send money, please contact Nooch support to complete our ID verification requirements. We apologize for the inconvenience but we are required to keep Nooch safe for all users!";
            }
            else if (msg.indexOf('Recepient have not linked to any bank account') > -1 || msg.indexOf('Recepient user details not found') > -1)
            {
                errorTitle = "Recipient's Bank Missing";
                errorMsg = "Looks like " + $scope.Details.RecepientName + " hasn't fully linked a bank account yet to receive payments.";
            }
            else if (msg.indexOf('Recipient has insufficient permissions: [UNVERIFIED]') > -1)
            {
                errorTitle = "Permission Error";
                errorMsg = "Looks like " + $scope.Details.RecepientName + " has an unverified account. Please ask them to contact Nooch support to complete our ID verification requirements. We apologize for the inconvenience but we have these policies to keep Nooch safe for everyone.";
            }
            else if (msg.indexOf("Recipient's bank has insufficient permissions:") > -1)
            {
                errorTitle = "Permission Error";
                errorMsg = "Looks like " + $scope.Details.RecepientName + " has an unverified bank account. Please ask them to contact Nooch support to complete the bank verification process. We apologize for the inconvenience but we have these policies to keep Nooch safe for everyone.";
            }
                // 2FA triggered from Synapse
            else if (msg.indexOf('Validation PIN sent to') > -1)
            {
                $scope.handle2fa();
                return;
            }
            else if (msg.indexOf('Member not found') > -1 || msg.indexOf('Missing') > -1)
            {
                errorTitle = "Unexpected Error";
                errorMsg = "Terrible sorry, looks like we had some trouble processing your payment. Please contact Nooch support for further help.";
            }
            else
            {
                errorTitle = "Unexpected Error 2";
                errorMsg = msg;//"Terrible sorry, looks like we had some trouble processing your payment. Please contact Nooch support for further help.";
            }

            swal({
                title: errorTitle,
                text: errorMsg,
                type: "error",
                showCancelButton: true,
                cancelButtonText: "Ok",
                confirmButtonText: "Contact Support",
                html: true
            }, function (isConfirm) {
                if (isConfirm)
                {
                    $cordovaSocialSharing
                      .shareViaEmail('', 'Nooch Payment Error - Help Request', 'support@nooch.com', null, null, null)
                      .then(function (res) {
                          $state.go('app.home');
                      }, function (err) {
                          console.log('Error attempting to send email from social sharing: [' + err + ']');
                          $state.go('app.home');
                      });
                }
                else if (goTo == "settings")
                {
                    $state.go('app.settings');
                }
            });
        }


        $scope.handle2fa = function () {
            console.log("Handle2FA() Reached!");
            swal({
                title: "New Device Detected",
                text: "Looks like you're using a new device. To double check this is an authorized payment, we just texted you a 6-digit code. Please enter the code to complete your payment.</span>" +
                      "<i class='show fa fa-mobile' style='font-size:40px; margin: 10px 0 0;'></i>",
                type: "input",
                inputPlaceholder: "ENTER CODE",
                showCancelButton: true,
                cancelButtonText: "Cancel",
                confirmButtonText: "Submit",
                closeOnConfirm: false,
                html: true,
                customClass: "heavierText"
            }, function (inputValue) {
                if (inputValue === false) return false;

                if (inputValue === "" || inputValue.length == 0)
                {
                    swal.showInputError("Please enter the code sent via SMS.");
                    return false
                }

                $ionicLoading.show({
                    template: 'Submitting code...'
                });

                enterPinService.submit2fa(inputValue)
                    .success(function (data) {
                        console.log(data);
                        $ionicLoading.hide();

                        if (data.isSuccess == true)
                        {
                            swal.close();
                            $scope.processPin();
                        }
                        else if (data.msg != null && data.msg.indexOf('Incorrect validation pin') > -1)
                        {
                            var text = data.msg.slice(data.msg.indexOf('Only '));
                            swal.showInputError("Incorrect code - " + text);
                            return false
                        }
                        else
                            $scope.showErrorAlert('Generic');
                    })
                    .error(function (error) {
                        console.log("Submit2fa Error: [" + error + "]");
                        $ionicLoading.hide();
                        swal("Error", error, "error");

                        if (error.ExceptionMessage == 'Invalid OAuth 2 Access')
                            CommonServices.logOut();
                    });
            });
        }


        $scope.GoBack = function () {
            if ($scope.returnUrl == 'app.howMuch')
            {
                console.log($scope.Details);

                // handle case when request is made to email address typed
                if (($scope.Details.SenderId == null || $scope.Details.SenderId == undefined) &&
					 $scope.Details.MoneySenderEmailId != null &&
					 $scope.Details.ContactNumber == null)
                    $state.go($scope.returnUrl, { recip: $scope.Details.MoneySenderEmailId });
                else if ($scope.Details.SenderId == null && $scope.Details.contactNumber != null)
                    $state.go($scope.returnUrl, { recip: $scope.Details.contactNumber });
                else
                    $state.go($scope.returnUrl, { recip: $scope.Details });
            }

            if ($scope.returnUrl == 'app.transferDetails')
                $state.go($scope.returnUrl, { trans: $scope.Details });

            $stateParams = '';
            $scope.Details = '';

            console.log("Enter PIN -> GoBack() -> Getting Here??");

            $ionicHistory.clearCache().then(function () { $state.go($scope.returnUrl); });
        };

    });
